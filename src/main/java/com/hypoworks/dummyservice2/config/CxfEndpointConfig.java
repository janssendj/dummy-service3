package org.wildfly.camel.examples.cdi;

import javax.inject.Named;
import javax.enterprise.inject.Produces;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.component.cxf.DataFormat;
import org.apache.deltaspike.core.api.config.ConfigProperty;

public class CxfEndpointConfig {
    
    @Produces()
    @Named("dummyService")
    public CxfEndpoint createCxfSoapEndpoint( @ConfigProperty(name = "wsdl.address") String publishedServerAddress) {
        CxfEndpoint cxfFromEndpoint = new CxfEndpoint();
        cxfFromEndpoint.setAddress(publishedServerAddress);
        cxfFromEndpoint.setWsdlURL("classpath:wsdl/DummyService.wsdl");
        cxfFromEndpoint.setDataFormat(DataFormat.PAYLOAD);
        cxfFromEndpoint.setAllowStreaming(false);
        return cxfFromEndpoint;

    }
}
