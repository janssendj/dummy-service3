package com.hypoworks.dummyservice2.config;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Destroyed;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.jms.pool.PooledConnectionFactory;
import org.apache.camel.component.jms.JmsConfiguration;

public class ActiveMQConfig {
/*
    public void init(@Observes @Initialized(ApplicationScoped.class) Object init,
            @Named("pooledConnectionFactory") PooledConnectionFactory pooledConnectionFactory) {
        pooledConnectionFactory.start();
    }

    public void destroy(@Observes @Destroyed(ApplicationScoped.class) Object init,
            @Named("pooledConnectionFactory") PooledConnectionFactory pooledConnectionFactory) {
        pooledConnectionFactory.stop();
    }
*/
    @Produces
    @Named("connectionFactory")
    @ApplicationScoped
    public ActiveMQConnectionFactory jmsConnectionFactory() {
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        factory.setUserName("admin");
        factory.setPassword("admin");

        return factory;
    }

    @Produces
    @Named("pooledConnectionFactory")
    @ApplicationScoped
    public PooledConnectionFactory createPooledConnectionFactory(@Named("connectionFactory") ActiveMQConnectionFactory factory) {
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setMaxConnections(8);
        pooledConnectionFactory.setConnectionFactory(factory);
        return pooledConnectionFactory;
    }

    @Produces
    @Named("jmsConfiguration")
    @ApplicationScoped
    public JmsConfiguration createJmsConfiguration(@Named("pooledConnectionFactory") PooledConnectionFactory pooledConnectionFactory) {
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(10);
        return jmsConfiguration;
    }

    @Produces
    @Named
    @ApplicationScoped
    public ActiveMQComponent createActiveMQComponent(@Named("jmsConfiguration") JmsConfiguration jmsConfiguration) {
        ActiveMQComponent component = new ActiveMQComponent();
        component.setConfiguration(jmsConfiguration);
        return component;
    }

}