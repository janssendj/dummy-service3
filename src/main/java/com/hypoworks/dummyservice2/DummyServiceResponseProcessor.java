package com.hypoworks.dummyservice2;

import com.hypoworks.dummyservice2.dummy.response.DummyResponse;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import org.apache.camel.cdi.ContextName;

@ApplicationScoped()
@Named()
public class DummyServiceResponseProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        DummyResponse dummyResponse = new DummyResponse();
        dummyResponse.setStatus("OK");
        exchange.getIn().setBody(dummyResponse);
    }
}
